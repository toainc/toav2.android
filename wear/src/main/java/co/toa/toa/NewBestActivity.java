package co.toa.toa;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class NewBestActivity extends Activity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wear_newbest);
        mTextView = (TextView) findViewById(R.id.text);
    }
}
