package co.toa.toa;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

import co.toa.toa.objects.MrEvent;
import co.toa.toa.utils.SirHandler;
import co.toa.toa.utils.converter.TGsonConverterFactory;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by gbern on 12/12/2015.
 */
public class EventsFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public EventsFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static EventsFragment newInstance() {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, 1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText("eventos " + getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
        Log.i(getClass().getName(), "1");
        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(SirHandler.interceptor);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SirHandler.BASE_URL)
                .addConverterFactory(TGsonConverterFactory.create())
                .client(client)
                .build();

        Log.i(getClass().getName(), "2");
        SirHandler handler = retrofit.create(SirHandler.class);
        Log.i(getClass().getName(), "3");
        Call<LinkedList<MrEvent>> call = handler.getEvents();

        Log.i(getClass().getName(), "4");
        call.enqueue(new Callback<LinkedList<MrEvent>>() {
            @Override
            public void onResponse(Response<LinkedList<MrEvent>> response, Retrofit retrofit) {
                //Got events!
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });

        Call<ResponseBody> calls = handler.getEventsStream();
        calls.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                try {
                    InputStream inputStream = response.body().byteStream();
                    Log.i(getClass().getName(), inputStream.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        return rootView;
    }
}