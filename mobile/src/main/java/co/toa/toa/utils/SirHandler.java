package co.toa.toa.utils;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.util.LinkedList;

import co.toa.toa.objects.MrComm;
import co.toa.toa.objects.MrEvent;
import co.toa.toa.objects.MrSport;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Streaming;

/**
 * Created by gbern on 12/30/2015.
 */
public interface SirHandler {

    String BASE_URL = "http://23.99.8.166/db/data/";

    Interceptor interceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request authRequest = originalRequest.newBuilder()
                    .addHeader("Authorization", "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==")
                    .addHeader("X-Stream", "true").build();
            return chain.proceed(authRequest);
        }
    };


    @GET("label/Sport/nodes")
    Call<LinkedList<MrSport>> getSports();

    @GET("label/Event/nodes")
    Call<LinkedList<MrEvent>> getEvents();

    @GET("label/Event/nodes")
    @Streaming
    Call<ResponseBody> getEventsStream();

    @GET("label/Community/nodes")
    Call<LinkedList<MrComm>> getCommunities();


}

