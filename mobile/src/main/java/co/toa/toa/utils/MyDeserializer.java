package co.toa.toa.utils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class MyDeserializer<T> implements JsonDeserializer<T> {

    @Override
    public T deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException {
        // Get the "content" element from the parsed JSON
        JsonElement content = je.getAsJsonObject().get("data");
        content.getAsJsonObject().addProperty("id", je.getAsJsonObject().get("metadata").getAsJsonObject().get("id").getAsInt());
        // Deserialize it. You use a new instance of Gson to avoid infinite recursion
        // to this deserializer
        return (new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()).fromJson(content, type);

    }
}