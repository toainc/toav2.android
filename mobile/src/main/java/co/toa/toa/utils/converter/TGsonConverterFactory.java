package co.toa.toa.utils.converter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import co.toa.toa.objects.MrEvent;
import co.toa.toa.utils.MyDeserializer;
import retrofit.Converter;

public final class TGsonConverterFactory extends Converter.Factory {
    private final Gson gson;

    private TGsonConverterFactory(Gson gson) {
        if (gson == null) throw new NullPointerException("gson == null");
        this.gson = gson;
    }

    /**
     * Create an instance using a default {@link Gson} instance for conversion. Encoding to JSON and
     * decoding from JSON (when no charset is specified by a header) will use UTF-8.
     */
    public static TGsonConverterFactory create() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(MrEvent.class, new MyDeserializer<MrEvent>());
        Gson gson = gsonBuilder.create();
        return create(gson);
    }

    /**
     * Create an instance using {@code gson} for conversion. Encoding to JSON and
     * decoding from JSON (when no charset is specified by a header) will use UTF-8.
     */
    public static TGsonConverterFactory create(Gson gson) {
        return new TGsonConverterFactory(gson);
    }

    @Override
    public Converter<ResponseBody, ?> fromResponseBody(Type type, Annotation[] annotations) {
        return new TGsonResponseBodyConverter<>(gson, type);
    }

    @Override
    public Converter<?, RequestBody> toRequestBody(Type type, Annotation[] annotations) {
        return new TGsonRequestBodyConverter<>(gson, type);
    }
}