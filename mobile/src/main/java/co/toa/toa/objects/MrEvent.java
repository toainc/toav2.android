package co.toa.toa.objects;

import com.google.android.gms.maps.model.Polyline;

import java.util.Date;

/**
 * Created by gbern on 12/12/2015.
 */
public class MrEvent implements Event {

    private String name, descr, address, organizer, cat, inscrplaces, prizes, imgurl, price;
    private int id;
    private double distance;
    private Date dateStart, dateEnd;
    private double X, Y;
    private Polyline ruta;

    public MrEvent() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getInscrplaces() {
        return inscrplaces;
    }

    public void setInscrplaces(String inscrplaces) {
        this.inscrplaces = inscrplaces;
    }

    public String getPrizes() {
        return prizes;
    }

    public void setPrizes(String prizes) {
        this.prizes = prizes;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public double getX() {
        return X;
    }

    public void setX(Long x) {
        X = x;
    }

    public double getY() {
        return Y;
    }

    public void setY(Long y) {
        Y = y;
    }

    public Polyline getRuta() {
        return ruta;
    }

    public void setRuta(Polyline ruta) {
        this.ruta = ruta;
    }

    @Override
    public void register() {

    }

    @Override
    public void unregister() {

    }
}
