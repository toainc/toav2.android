package co.toa.toa.objects;

/**
 * Created by gbern on 12/12/2015.
 */
public interface Event {

    void register();

    void unregister();


}
