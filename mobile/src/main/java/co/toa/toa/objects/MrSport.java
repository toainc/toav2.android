package co.toa.toa.objects;

/**
 * Created by gbern on 12/30/2015.
 */
public class MrSport {
    private String icnurl;
    private String icnurl_alt;
    private String bgurl;
    private String name;

    public MrSport(String icnurl, String icnurl_alt, String bgurl, String name) {
        this.icnurl = icnurl;
        this.icnurl_alt = icnurl_alt;
        this.bgurl = bgurl;
        this.name = name;
    }

    public String getIcnurl() {
        return icnurl;
    }

    public String getIcnurl_alt() {
        return icnurl_alt;
    }

    public String getBgurl() {
        return bgurl;
    }

    public String getName() {
        return name;
    }
}
